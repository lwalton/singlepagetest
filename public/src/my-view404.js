define(["../node_modules/@polymer/polymer/polymer-element.js"],function(_polymerElement){"use strict";/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */class MyView404 extends _polymerElement.PolymerElement{static get template(){return _polymerElement.html`
      <style>
        :host {
          display: block;

          padding: 10px 20px;
        }
      </style>
      <h1>404</h1>
      Looks like you've found nothing...
    `}}window.customElements.define("my-view404",MyView404)});