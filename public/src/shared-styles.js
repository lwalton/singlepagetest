define(["../node_modules/@polymer/polymer/polymer-element.js"],function(_polymerElement){"use strict";const $_documentContainer=document.createElement("template");$_documentContainer.innerHTML=`<dom-module id="shared-styles">
  <template>
    <style>
    :root {
      --the-green: #0c2b16;
      --the-red: #90021f;
      --primary-text-color: #1a1a1a;
      --paper-input-container-color: var(--the-green);
      --paper-input-container-focus-color: var(--the-green);
      --paper-input-container-invalid-color: var(--the-red);
      --paper-input-container-focus-color: var(--the-green);
    }

      .circle {
        display: inline-block;
        width: 64px;
        height: 64px;
        text-align: center;
        color: #555;
        border-radius: 50%;
        background: #ddd;
        font-size: 30px;
        line-height: 64px;
      }

      h1 {
        margin: 16px 0;
        color: #212121;
        font-size: 22px;
      }
      
      .card img{
        max-width: 100%;
      }

      .none{
        display:none !important;
      }

      .none{
        display: none !important;
      }


/* Firefox < 16 */
@-moz-keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}

/* Safari, Chrome and Opera > 12.1 */
@-webkit-keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}

/* Internet Explorer */
@-ms-keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}

/* Opera < 12.1 */
@-o-keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}

@keyframes fadein {
  from { opacity: 0; }
  to   { opacity: 1; }
}

    </style>
  </template>
</dom-module>`;document.head.appendChild($_documentContainer.content)});