define(["../node_modules/@polymer/polymer/polymer-element.js","./shared-styles.js"],function(_polymerElement,_sharedStyles){"use strict";class MyGiftlist extends _polymerElement.PolymerElement{static get template(){return _polymerElement.html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>

      <div class="card">
         <h1>Giftlist</h1>
      </div>
    `}}window.customElements.define("my-giftlist",MyGiftlist)});