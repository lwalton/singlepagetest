define(["../node_modules/@polymer/polymer/polymer-element.js","./shared-styles.js"],function(_polymerElement,_sharedStyles){"use strict";class MyWhen extends _polymerElement.PolymerElement{static get template(){return _polymerElement.html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>
       <h1>When?</h1>

     
    `}}window.customElements.define("my-when",MyWhen)});