define(["../node_modules/@polymer/polymer/polymer-element.js","./shared-styles.js","../node_modules/@polymer/paper-spinner/paper-spinner.js","../node_modules/firebase/app/dist/index.esm.js","../node_modules/firebase/database/dist/index.esm.js","../node_modules/@polymer/iron-a11y-keys/iron-a11y-keys.js","../node_modules/@polymer/iron-icons/iron-icons.js"],function(_polymerElement,_sharedStyles,_paperSpinner,_indexEsm,_indexEsm2,_ironA11yKeys,_ironIcons){"use strict";_indexEsm=babelHelpers.interopRequireDefault(_indexEsm);class MySaveTheDate extends _polymerElement.PolymerElement{static get template(){return _polymerElement.html`
     <style include="shared-styles">
      
      
.save-header {
    margin: 0;
    max-width: 100%;
    font-weight: normal;
    color: #ffffff;
}

.save-header h2 {
    padding: 0px;
}

paper-button {
    width: 100%;
    background-color: var(--the-green);
    font-family: "Open Sans", sans-serif;
    text-transform: capitalize;
    color: white;
    margin: 0;
    position: absolute;
    line-height: 100%;
    bottom: 0;
    height: 100%;
    border-radius: 0;
}

.save-text {
    bottom: 0px;
}

* {
    color: #1a1a1a;
}

a {
    color: #1a1a1a;
}

.save-img {
    cursor: pointer;
    width: 100%;
    padding: 0;
    object-fit: cover;
}

.save-top {
    border: 14px var(--thegreen) double;
    height: calc(40% - 24px);
    padding: 24px;
    padding-bottom: 0px;
    position: relative;
}

.save-middle {
    height: auto;
    width: 100%;
    padding: 0;
}

.save-bottom {
    height: 12%;
    width: 100%;
    padding: 0;
    background-color: var(--the-green);
    color: #fff;
}

h2 {
    font-family: "Special Elite", sans-serif;
    text-align: left;
    font-size: 48px;
    line-height: 1;
}

h5 {
    font-family: "Open Sans", sans-serif;
    font-weight: 200;
    font-size: 16px;
}

.container {
    height: 100%;
    width: 100%;
    position: absolute;
    top: 0;
}

paper-input {
    margin: 24px;
}

.card {
    max-width: calc((100vh * (5 / 8) - 48px)) !important;
    width: calc(100% - 48px);
    height: calc((100vw - 48px) *8 / 5);
    position: relative;
    top: 0;
    left: 0;
    margin-top: 24px;
    margin-left: auto;
    margin-right: auto;
}

.card__front,
.card__back {
    backface-visibility: hidden;
    -webkit-backface-visibility: hidden;
    height: 100%;
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    right: 0;
    width: 100%;
    transition: transform 1.25s;
    border: 1px #aab8bb solid;
    background: white;
    box-shadow: 0 19px 38px rgba(0, 0, 0, 0.3), 0 15px 12px rgba(0, 0, 0, 0.22);
}

.flip {
    transform: rotateY(180deg);
}

.bottom {
    position: absolute;
    bottom: 0;
    height: 11%;
    width: 100%;
    padding: 0;
    background-color: var(--the-green);
    color: #fff;
}

.email-text {
    margin: 24px;
}

.email-top {
    background: var(--the-green);
    color: white;
    padding: 24px;
}


.smaller-text {
    font-size: 30px !important;
}

paper-button[disabled] {
    cursor: not-allowed;
    background: #b3b2b8;
    color: #ffffff;
}

.full {
    width: 100%;
    height: 100%;
    background: rgba(192, 192, 192, 0.3);
    position: absolute;
    top: 0;
}

paper-spinner {
    top: calc(50% + 24px);
    left: calc(50% - 24px);
}

.succ_hide {
    bottom: 0;
    top: 0;
    position: absolute;
    max-height: 100%;
    width: 100%;
    background: var(--the-green);
    transition: all 0.8s linear;
}

#big_button {
    display: none;
}

.gro {
    height: calc(48% - 48px);
}

h5 {
    margin: 4px;
}

.abs {
    position: absolute;
}

.hide>* {
    display: none;
}

.hide>paper-spinner {
    display: block;
}

.h70 {
    height: 70% !important;
}

iron-icon {
    color: white;
    position: absolute;
    top: 12px;
    right: 12px;
    cursor: pointer;
}

@media only screen and (max-width: 380px) {
    h2 {
        font-size: 48px;
        margin-bottom: 0;
    }

    h5 {
        margin-top: 0 !important;
        font-size: 14px;
    }

    .save-header {
        font-size: 32px;
    }
}

@media only screen and (max-width: 370px) {
    .save-text {
        bottom: 16px;
        left: 16px;
    }

    .card {
        width: calc(100% - 48px);
        top: 0;
        left: 0;
    }
}

@media only screen and (min-width: 550px) {
    .card {
        max-width: 500px;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
    }

    .card {
        height: calc(95% - 48px);
    }
}

#coverer {
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    height: 100%;
    width: 100%;
    background-color: #fff;
    z-index: 10;

    -webkit-animation: fadein 0.5s; /* Safari, Chrome and Opera > 12.1 */
       -moz-animation: fadein 0.5s; /* Firefox < 16 */
        -ms-animation: fadein 0.5s; /* Internet Explorer */
         -o-animation: fadein 0.5s; /* Opera < 12.1 */
            animation: fadein 0.5s;
       
}

#coverer img {
    max-width: 50%;
    max-height: 50%;
    text-align: center;
    display: block;
    margin: auto;
}

#coverer iron-icon {
    color: var(--the-green);
}

#coverer h5 {
    color: var(--the-green);
    text-align: center;
    margin: auto;
}

.bg_rings {
    width: 20%;
    opacity: 60%;
    position: absolute;
    bottom: 12%;
    right: 1%;
}

.cookie {
    -webkit-user-select: none;
    /* Safari */
    -moz-user-select: none;
    /* Firefox */
    -ms-user-select: none;
    /* IE10+/Edge */
    user-select: none;
    /* Standard */
    bottom: 0;
    position: absolute;
    right: 0;
    font-size: 12px;
    color: #bbb;
    margin: 8px;
}

</style>
<app-location route="{{_route}}"></app-location>
<div class="container">
    <div class="card" id="card">
        <div id="front" class="card__front hide">
            <paper-spinner id="spin1" active></paper-spinner>
            <div class="save-middle" id="sm" on-click="clicky">
                <picture>
                    <source
                        srcset="../../images/bigbg.webp 1600w, ../../images/medbg.webp 1000w, ../../images/smallbg.webp 500w, "
                        type="image/webp" />
                    <source
                        srcset="../../images/bigbg.png 1600w, ../../images/medbg.png 1000w, ../../images/smallbg.png 500w, "
                        type="image/png" />
                    <img class="save-img" src="../../images/medbg.png" alt="We're engaged!" />
                </picture>
            </div>
        </div>

        <div id="back" class="card__back flip" id="full_thingy">
            <div class="email-top">
                <iron-icon class="exit" icon="close" on-click="clicky"></iron-icon>
                <h2 id="names_box" class="save-header">Dear [[_names]],</h2>
            </div>
            <div>
                <div id="coverer" class="none">
                    <iron-icon class="exit" icon="close" on-click="clicky"></iron-icon>

                    <img src="../../images/RINGS.svg" />
                    <h5>[[_succ]]</h5><br />
                    <h5>Any problems? <a href="mailto:us@lukeandgeorgi.co.uk?subject=Your website is amazing!">Email
                            us</a><br /><br />
                        <a href="http://www.google.com/calendar/render?
                            action=TEMPLATE
                            &text=Luke+and+Georgi's+Wedding
                            &dates=20200509/20200509
                            &details=Luke+and+Georgi+are+getting+married!
                            &location=Bromley,+Kent
                            &trp=false
                            &sprop=
                            &sprop=name:" target="_blank" rel="noopener">Add to Google Calendar</a>
                </div>
                <h5 class="email-text">
                    We would love for you to join us for our special day.<br /> <br />
                    Please enter your email address so that we can send you an invitation. <br />
                </h5>
                <iron-a11y-keys id="a11y" target="[[target]]" keys="enter" on-keys-pressed="onEnter"></iron-a11y-keys>
                <paper-input invalid="{{_valid}}" auto-validate value="{{_email}}" label="Email address" id="email"
                    pattern="[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}" required>
                    {{_email}}
                </paper-input>
                <img class="bg_rings" src="../../images/RINGS.svg" alt="Rings" />
            </div>
            <div class="bottom">
                <paper-button id="sub_button" disabled="[[_valid]]" on-click="sumbitty">
                    Submit
                </paper-button>
            </div>
            <div id="spin_div" class="full none">
                <paper-spinner active></paper-spinner>
            </div>
        </div>
    </div>
</div>
<div class="cookie">This website uses cookies</div>
</body>
`}static get properties(){return{// This is the data from the store.
_route:Object,_database:Object,_uid:String,_names:Array,_email:{type:String},_done:{type:Boolean,value:/*tagged=*/ /*tagged=*/ /*fullyInitialized=*/ /*filtered=*/ /*fullyInitialized=*/ /*filtered=*/ /*isMerge*/ /*isMerge=*/ /*forceRefresh=*/!1},_clicked:{type:Boolean,value:!1},_valid:{type:Boolean,value:/*orderByCalled=*/ /*orderByCalled=*/ /*orderByCalled=*/ /*orderByCalled=*/ /*fromUser=*/ /*fromServer=*/ /*fromServer=*/ /*tagged=*/ /*isMerge=*/ /*asynchronous=*/ /*export=*/ /*export=*/ /*export=*/ /*includeSelf=*/!0},_succ:String}}onEnter(){if(!this._valid&&!this._done){this.sumbitty()}}setup(){var stn;this.new_front();var x=this._database.screenName;if(x.includes(";")){x=x.split(";");for(var stn="",i=0;i<x.length;i++){if(i==x.length-1){stn+=" &"}else if(0<i){stn+=", "}stn+=x[i]}}else{stn=x}if(8<stn.length)this.$.names_box.classList.add("smaller-text");this._names=stn;this.$.front.classList.remove("hide");this.$.spin1.classList.add("none");var viewed=this._database.viewed;if(!viewed){viewed=-1}viewed++;_indexEsm.default.database().ref("db2/"+this._uid+"/viewed").set(viewed);// firebase.database().ref("DB/" + this._uid + "/viewed").set(viewed);
}connectedCallback(){super.connectedCallback();this._uid=this._route.__queryParams.uid;if(!this._uid){document.location.href="404"}else{const firebaseConfig={apiKey:"AIzaSyAb8PwGNKYKD2B1Gja62RjvDZdL8CG94uU",authDomain:"luke-and-georgi.firebaseapp.com",databaseURL:"https://luke-and-georgi.firebaseio.com",projectId:"luke-and-georgi",storageBucket:"luke-and-georgi.appspot.com",messagingSenderId:"590135720352",appId:"1:590135720352:web:2422263a85c94772"};_indexEsm.default.initializeApp(firebaseConfig);var that=this;return _indexEsm.default.database().ref("/db2/"+this._uid).once("value").then(function(snapshot){// return firebase.database().ref('DB/' + this._uid).once('value').then(function (snapshot) {
that._database=snapshot.val();that.setup()})}}clicky(){this._email=this._database.email;this.$.front.classList.toggle("flip");this.$.back.classList.toggle("flip");this.$.coverer.classList.add("none")}sumbitty(){var eme=this._email;this.dispatchEvent(new CustomEvent("email",{eme,bubble:!0,composed:!0}));this._done=!0;this.$.spin_div.classList.remove("none");this.new_front();var that=this;_indexEsm.default.database().ref("db2/"+this._uid+"/email").set(// firebase.database().ref("DB/" + this._uid + "/email").set(
that._email,function(error){that.$.coverer.classList.remove("none");if(error){console.log(error);that._succ="An error occured. Please try again"}else{const oneDay=1e3*(60*(60*24)),today=new Date,theDate=new Date(2020,4,10),countdown=Math.round(Math.abs((today-theDate)/oneDay));that._succ="Email saved; we can't wait to see you in "+countdown+" days!";that.$.spin_div.classList.add("none")}})}//For when they've already submitted their details
new_front(){this.$.spin1.classList.add("none");this.$.front.classList.remove("hide")}}window.customElements.define("my-save-the-date",MySaveTheDate)});