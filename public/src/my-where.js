define(["../node_modules/@polymer/polymer/polymer-element.js","./shared-styles.js"],function(_polymerElement,_sharedStyles){"use strict";class MyWhere extends _polymerElement.PolymerElement{static get template(){return _polymerElement.html`
      <style include="shared-styles">
        :host {
          display: block;
          padding: 10px;
        }

        .card .stuff{
          padding:16px;
        }
      </style>
      <h1>Where?</h1>
      <div class="card">
        <img src="https://kingschurchlondon.org/assets/img/history_11_780.jpg">
        <div class="stuff">
        <h1>Ceremony</h1>
        <p>Kings Church Downham</p>
        <p>This is a church. It is kinda nice. It is where our wedding is happening.</p>
          <a href="https://goo.gl/maps/J7wsQaWC7QcgfBnY8">Directions</a>
      </div>
      </div>
      </div>

      <div class="card">
        <img src="https://s0.geograph.org.uk/geophotos/02/64/25/2642513_e713f52f.jpg">
        <div class="stuff">
        <h1>Recpetion</h1>
        <p>Keston Villiage Hall</p>
        <p>This is a hall room thing. It is kinda nice. It's also where our wedding is happening.</p>
          <a href="https://g.page/kestonvillagehall?share">Directions</a>
      </div>
      </div>
      </div>
    `}}window.customElements.define("my-where",MyWhere)});