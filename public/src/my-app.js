define(["require","../node_modules/@polymer/polymer/polymer-element.js","../node_modules/@polymer/polymer/lib/utils/settings.js","../node_modules/@polymer/app-layout/app-drawer/app-drawer.js","../node_modules/@polymer/app-layout/app-drawer-layout/app-drawer-layout.js","../node_modules/@polymer/app-layout/app-header/app-header.js","../node_modules/@polymer/app-layout/app-header-layout/app-header-layout.js","../node_modules/@polymer/app-layout/app-scroll-effects/app-scroll-effects.js","../node_modules/@polymer/app-layout/app-toolbar/app-toolbar.js","../node_modules/@polymer/app-route/app-location.js","../node_modules/@polymer/app-route/app-route.js","../node_modules/@polymer/iron-pages/iron-pages.js","../node_modules/@polymer/iron-selector/iron-selector.js","../node_modules/@polymer/paper-icon-button/paper-icon-button.js","../node_modules/@polymer/paper-button/paper-button.js","../node_modules/@polymer/paper-input/paper-input.js","./my-icons.js","../node_modules/firebase/app/dist/index.esm.js","../node_modules/firebase/database/dist/index.esm.js"],function(_require,_polymerElement,_settings,_appDrawer,_appDrawerLayout,_appHeader,_appHeaderLayout,_appScrollEffects,_appToolbar,_appLocation,_appRoute,_ironPages,_ironSelector,_paperIconButton,_paperButton,_paperInput,_myIcons,_indexEsm,_indexEsm2){"use strict";_require=babelHelpers.interopRequireWildcard(_require);_indexEsm=babelHelpers.interopRequireDefault(_indexEsm);// Gesture events like tap and track generated from touch will not be
// preventable, allowing for better scrolling performance.
(0,_settings.setPassiveTouchGestures)(/*orderByCalled=*/ /*orderByCalled=*/ /*orderByCalled=*/ /*orderByCalled=*/ /*fromUser=*/ /*fromServer=*/ /*fromServer=*/ /*tagged=*/ /*isMerge=*/ /*asynchronous=*/ /*export=*/ /*export=*/ /*export=*/ /*includeSelf=*/!0);// Set Polymer's root path to the same value we passed to our service worker
// in `index.html`.
(0,_settings.setRootPath)(MyAppGlobals.rootPath);class MyApp extends _polymerElement.PolymerElement{static get template(){return _polymerElement.html`
      <style>
        .none {
          display: none  !important;
        }

        app-drawer .contents{
          height:100%;
          background-color: #0c2b16;
          font-family: "Open Sans", sans-serif;
          flex:1;
        }

        app-drawer img{
          max-width: 60%;
        }

       .title{
          width:100%;
          text-align:center;
          margin-bottom:0;
          font-size: 26px;
          font-family: "Special Elite", serif;
        }

        .drawer-image-contatiner{
          text-align: center;
        }

        .container{
          display: flex;
		      flex-direction: column;
          height:100%;
        }

        .container iron-selector a,
        .container iron-selector a:visited{
          width: 100%;
          text-decoration: none;
          color:white;
        }

        .container iron-selector a div{
          width: 100%;
          padding: 16px;
          font-size: 20px;
        }

        app-header{
          background-color: #0c2b16;
          color: white;
        }
      </style>

      <app-location route="{{route}}" url-space-regex="^[[rootPath]]"/>
      </app-location>

      <app-route
        route="{{route}}"
        pattern="[[rootPath]]:page"
        data="{{routeData}}"
        tail="{{subroute}}">
      </app-route>

      <!-- Main content -->
      <app-drawer-layout fullbleed="" narrow="{{narrow}}">
        <app-drawer   slot="drawer" swipe-open="[[narrow]]" id="drawer">
        <div class="container">
          <p class="title">
            Luke & Georgi
          </p>
          <div class="drawer-image-contatiner">
            <img src="../images/rings.svg"/>
          </div>
          <div class="contents">
            <iron-selector selected="[[page]]" attr-for-selected="name" class="drawer-list" role="navigation">
              <a name="view1" href="[[rootPath]]">
                <div>Home</div>
              </a>
              <a name="when" href="[[rootPath]]when">
                <div>When</div>
              </a>
              <a name="where" href="[[rootPath]]where">
                <div>Where</div>
              </a>
              <a name="giftlist" href="[[rootPath]]giftlist">
                <div>Giftlist</div>
              </a>
              <a name="contact" href="[[rootPath]]contact">
                <div>Contact Us</div>
              </a>
            </iron-selector>
          </div>
         </div>
       </app-drawer>

        <!-- Main content -->
     <app-header-layout has-scrolling-region="">
          <app-header
            slot="header"
            condenses=""
            reveals=""
            effects="waterfall"
            id="app_header">
            
            <app-toolbar>
              <paper-icon-button
                icon="my-icons:menu"
                drawer-toggle=""
              ></paper-icon-button>
              <div main-title="" class="title">Luke & Georgi</div>
              <paper-icon-button
              ></paper-icon-button>
            </app-toolbar>
          </app-header> 


          <iron-pages selected="[[page]]" attr-for-selected="name" role="main">
            <my-view1 name="view1"></my-view1>
            <my-save-the-date  id="save" name="save-the-date"></my-save-the-date>
            <my-when name="when"></my-when>
            <my-where name="where"></my-where>
            <my-giftlist name="giftlist"></my-giftlist>
            <my-contact name="contact"></my-contact>
            <my-view404 name="view404"></my-view404>
          </iron-pages>
        </app-header-layout>
      </app-drawer-layout>
    `}static get properties(){return{page:{type:String,reflectToAttribute:!0,observer:"_pageChanged"},routeData:Object,subroute:Object,_email:String}}ready(){super.ready();this.addEventListener("email",e=>this.setCookie())}connectedCallback(){super.connectedCallback();this._email=this.getCookie();if(this._email===void 0||null===this._email){//TODO login screen
}}static get observers(){return["_routePageChanged(routeData.page)"]}_routePageChanged(page){// Show the corresponding page according to the route.
//
// If no page was found in the route data, page will be an empty string.
// Show 'view1' in that case. And if the page doesn't exist, show 'view404'.
if(!page){// this.page = "view1";
// this.$.drawer.close();
//TODO change this back
this._404()}else if(-1!==["view1",,"when","where","giftlist","contact"].indexOf(page)){// this.page = page;
// this.$.drawer.close();
//TODO change this back
this._404()}else if(-1!==["save-the-date"].indexOf(page)){this.page=page;this.$.app_header.classList.add("none");this.$.drawer.remove()}else{this._404()}}_404(){this.page="view404";this.$.app_header.classList.add("none");this.$.drawer.remove()}setCookie(){var name="email",value=this.$.save._email,expires="",day=new Date(16281612e5),date=new Date;date.setTime(day);expires="; expires="+date.toUTCString();document.cookie=name+"="+(value||"")+expires+"; path=/"}getCookie(){for(var nameEQ="email"+"=",ca=document.cookie.split(";"),i=0,c;i<ca.length;i++){c=ca[i];while(" "==c.charAt(0))c=c.substring(1,c.length);if(0==c.indexOf(nameEQ))return c.substring(nameEQ.length,c.length)}return null}_pageChanged(page){// Import the page component on demand.
//
// Note: `polymer build` doesn't like string concatenation in the import
// statement, so break it up.
switch(page){case"view1":new Promise((res,rej)=>_require.default(["./my-view1.js"],res,rej));break;case"save-the-date":new Promise((res,rej)=>_require.default(["./my-save-the-date.js"],res,rej));break;case"when":new Promise((res,rej)=>_require.default(["./my-when.js"],res,rej));break;case"where":new Promise((res,rej)=>_require.default(["./my-where.js"],res,rej));break;case"giftlist":new Promise((res,rej)=>_require.default(["./my-giftlist.js"],res,rej));break;case"contact":new Promise((res,rej)=>_require.default(["./my-contact.js"],res,rej));break;case"view404":new Promise((res,rej)=>_require.default(["./my-view404.js"],res,rej));break;}}}window.customElements.define("my-app",MyApp)});